
INSTALLATION
============
Before you enable the Leaflet Maptiler module, you need to download and enable
the Leaflet module and the Libraries module.

If all's ok, you won't see any errors in the Status Report admin/reports/status.

After this all you have to do is enable Leaflet Maptiler to enhance your
mapping experience using maps from Maptiler.

You select the map when you format a single field (eg Geofield) as a
map or when you format a View (of multiple nodes or users) as a map. The module
"IP Geolocation Views and Maps" is particularly good for this.

You can configure Maptiler settings at: admin/config/leaflet_maptiler. Here you
should set your Maptiler API Key, layers and the attribution links. If you set
more than 1 layer for your map, a layer switcher will automatically appear
in the upper right-hand corner.